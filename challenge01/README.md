# Data manipulation challenge

This challenge is divided in parts but all are based on the same base scenario, 6 branch routers connected to each other. The goal of this excercise is to practice Python data structures.

## Part 1: Cutsheet and device facts

The goal is to parse a Cutsheet-style CSV file (`cutsheet.csv`) and create a data structure that holds the link data. The data structure at the end should:

- No ports being reused on a single router. For example it should not have duplicate links data.
- Have a valid interface name and value.

The number of interfaces available on each device is depicted in `facts.yml`, you should be able to use this information as a constraint for the interface name and value specified above.

**NOTE:** Is important to highlight that you should treat the `cutsheet.csv` file as a manually generated file prone to errors, while the `facts.yml` comes from a source of truth like a database (meaning that the data there is already validated).

## Part 2: API integration with NetBox

For this part you will need to install the `netbox-docker` project to follow along.

- Use this [guide](https://github.com/netbox-community/netbox-docker/wiki/Getting-Started) to install. Follow all the steps.
  - The project uses `docker`, `docker-compose` and `git` to be installed on you system.
  - Since it uses `docker-compose` it will fire up multiple containers. The data you insert in the system will remain there if you issue the normal `docker-compose up` and `docker-compose down`.
  - **NOTE:** If you delete the containers at a later stage remeber that your data will be lost.
- Start using the API

Now, there are 2 objectives in this part.

### Part 2a: Update Netbox with contents from facts.yml

Follow these steps to add the same data as your `facts.yml`:

- Create 2 **Sites**, named `Branch A` with ASN 65444 and `Branch B` with ASN 65777.
- Next create 2 **Manufacturers**, named `Arista` and `Cisco`.
- Next create 2 **Platforms**, named `eos` with `Arista` as manufacturer and `ios` with `Cisco` as manufacturer.
- Next create 2 **Device Type**, they will be named both `virtual-router` but will differ on the manufacturer (one will use `Arista` and the other will use `Cisco`).
  - On each you will add interfaces. Click on **+Add Components** and select **interfaces**.
  - For the `Cisco` device type, the interfaces pattern you will use is `Gi[0-1]/[0-3]`.
  - For the `Arista` device type, the interfaces pattern you will use is `Eth[0-1]/[0-3]`.
- Next create 2 **Device Roles**, one will be named `AGG` and the other will be named `BORDER`. Select the colors that you want.
- Finally create 6 **Devices**:
  - `Branch A` will have: `r1`, `r2` and `r3`. These will be `Arista` as **Manufacturer** and `virtual-router` as **Device Type**
  - `Branch A` will have: `r4`, `r5` and `r6`. These will be `Cisco` as **Manufacturer** and `virtual-router` as **Device Type**
  - `r1` and `r6` will be of role `AGG`, while the rest will be `BORDER`.

### Part 2b: Update links with the Netbox API

TBD
