#!/usr/bin/env python
"""
Script that performs tasks depicted on challenge1 but with an OOP approach
"""
from collections import UserList
import yaml
import csv
import reprlib
from pathlib import Path
from dataclasses import dataclass
from typing import Any, ChainMap, Dict, Iterable, List
from pprint import pprint


CUTSHEET = Path("../cutsheet.csv")
FACTS = Path("../facts.yml")


def read_yaml(path: Path) -> List[Dict[str, Any]]:
    with open(path, "r") as f:
        data = yaml.load(f, Loader=yaml.FullLoader)
    return data


def read_csv(path: Path) -> List[List[str]]:
    data = []
    with open(path, "r") as f:
        data_reader = csv.reader(f,)
        for row in data_reader:
            data.append([item.strip("\ufeff") for item in row])
    return data


def interface_converter(name: str, net_os: str):
    """
    Bases on the name and net_os, it returns the proper interface name.
    """
    if net_os == "ios":
        return name.replace("g", "Gi")
    elif net_os == "eos":
        return name.replace("g", "Eth")
    else:
        raise ValueError("Interface not understood")


@dataclass(frozen=True)
class Link:
    """
    Link dataclass
    """

    device_a: str
    interface_a: str
    device_b: str
    interface_b: str


class Links(UserList):
    """
    List-like class that implements a verifier method when adding a new Link.
    """

    @staticmethod
    def verifier(link1: Link, link2: Link) -> None:
        """
        It verifies if the interface for an endpoint is already used
        """
        if (
            link1.device_a == link2.device_a and link1.interface_a == link2.interface_a
        ) or (
            link1.device_a == link2.device_b and link1.interface_a == link2.interface_b
        ):
            raise ValueError(f"Link is already used: {link2}")
        elif (
            link1.device_b == link2.device_a and link1.interface_b == link2.interface_a
        ) or (
            link1.device_b == link2.device_b and link1.interface_b == link2.interface_b
        ):
            raise ValueError(f"Link is already used: {link2}")

    def _link_verifier(self, value):
        if not isinstance(value, Link):
            raise ValueError(f"Value is not of Link type: {value}")
        for _link in self.data:
            self.verifier(_link, value)

    def append(self, value: Link) -> None:
        self._link_verifier(value)
        super().append(value)

    def extend(self, values: Iterable) -> None:
        for value in values:
            self._link_verifier(value)
        super().extend(values)

    def insert(self, index: int, value: Link) -> None:
        self._link_verifier(value)
        super().insert(index, value)


@dataclass
class Device:
    """
    Device dataclass
    """

    device: str
    model: str
    net_os: str
    interfaces: List[str]
    # _links: Set[Link] = field(default_factory=set)

    # @property
    # def links(self):
    #     print("Getting value")
    #     print(type(self))
    #     to_remove = []
    #     for item in self._links:
    #         if not isinstance(item, Link):
    #             print("removiendo")
    #             to_remove.append(item)
    #     if to_remove:
    #         [self._links.remove(item) for item in to_remove]
    #     return self._links

    # @links.setter
    # def links(self, value: Link) -> None:
    #     print("Setting value")
    #     self._links.add(value)


class Devices(ChainMap):
    """
    Dictionary-like class that holds each device name as key, and a Device dataclass as
    value
    """

    def __init__(self, *args, **kwargs):
        for arg in args:
            for value in arg.values():
                if not isinstance(value, Device):
                    raise ValueError(f"Value is not of Device type. {value}")
        super().__init__(*args, **kwargs)

    def __setitem__(self, k: str, v: Device) -> None:
        if not isinstance(v, Device):
            raise ValueError(f"Value is not of Device type. {v}")
        super().__setitem__(k, v)

    @reprlib.recursive_repr()
    def __repr__(self) -> str:
        return f"{self.__class__.__name__}(" + ", ".join(map(repr, self)) + ")"


def validate_interface(name: str, device: Device) -> str:
    """
    Returns the standardized interface name and it checks if is under the available for
    that endpoint
    """
    _interface = interface_converter(name, net_os=device.net_os)
    if _interface not in device.interfaces:
        raise TypeError(
            f"[ERROR] Interface {_interface} not valid for {device.device} - see "
            "available interfaces in facts.yml"
        )
    return _interface


def validate_endpoint(
    endpoint_a: List[str], endpoint_b: List[str], devices: Devices
) -> None:
    """
    It validates each endpoint against the constraints given from the respective
    endpoint Device
    """
    for endpoint in [endpoint_a, endpoint_b]:
        endpoint[-1] = validate_interface(endpoint[-1], devices[endpoint[0]])


def links_fabric(
    cutsheet: List[List[str]], devices: Devices, skip_errors: bool = False
) -> Links:
    """
    Creates a clean Links() data structure from a cutsheet data gotten from `read_csv`.

    It uses the Devices() data structure to determine the constraints to validate each
    endpoint, this would mean devices and interfaces.

    By default it will raise any errors it encounters while generating the Links, but
    if skip_errors is set to true, it will ignore the invalid endpoints and continue.
    """
    _links = Links()
    for _clink in cutsheet:
        endpoint_a = _clink[:2]
        endpoint_b = _clink[-2:]

        if skip_errors:
            try:
                validate_endpoint(endpoint_a, endpoint_b, devices)
                _links.append(
                    Link(endpoint_a[0], endpoint_a[-1], endpoint_b[0], endpoint_b[-1])
                )
            except KeyError:
                continue
            except TypeError as err:
                if "see available interfaces in facts.yml" in str(err):
                    continue
                else:
                    raise
            except ValueError as err:
                if "Interface value not understood" in str(
                    err
                ) or "Link is already used" in str(err):
                    continue
                else:
                    raise
        else:
            validate_endpoint(endpoint_a, endpoint_b, devices)
            _links.append(
                Link(endpoint_a[0], endpoint_a[-1], endpoint_b[0], endpoint_b[-1])
            )

    return _links


if __name__ == "__main__":
    # Load data from cutsheet and facts
    facts = read_yaml(FACTS)
    cutsheet = read_csv(CUTSHEET)

    # Construct Devices
    devices = Devices({dev["device"]: Device(**dev) for dev in facts})

    # Construct Links
    links = links_fabric(cutsheet, devices, skip_errors=True)
    pprint(devices)
    pprint(links)
