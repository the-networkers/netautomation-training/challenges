import pytest
from .ch1_oop import *  # noqa


def test_read_yaml():
    facts = read_yaml(FACTS)  # noqa
    assert facts[0] == {
        "device": "r1",
        "model": "virtual-router",
        "net_os": "ios",
        "interfaces": [
            "Gi0/0",
            "Gi0/1",
            "Gi0/2",
            "Gi0/3",
            "Gi1/0",
            "Gi1/1",
            "Gi1/2",
            "Gi1/3",
        ],
    }


def test_read_csv():
    cutsheet = read_csv(CUTSHEET)  # noqa
    assert cutsheet == [
        ["r1", "g0/0", "r2", "g0/0"],
        ["r1", "g0/1", "r3", "g0/0"],
        ["r2", "g0/0", "r3", "g0/0"],
        ["r2", "g0/1", "r3", "g0/1"],
        ["r3", "g0/1", "r2", "g0/1"],
        ["r3", "g0/11", "r2", "g0/1"],
        ["r3", "g1/0", "r4", "g1/0"],
        ["r2", "g1/0", "r5", "g1/0"],
        ["r3", "g1/0", "r4", "g1/0"],
        ["r4", "g0/1", "r5", "g0/1"],
        ["r6", "g0/0", "r4", "g0/0"],
        ["r6", "g0/1", "r5", "g0/0"],
        ["r6", "g20/1", "r5", "g0/0"],
        ["r7", "g0/1", "r88", "g0/0"],
    ]


def test_interface_converter():
    assert "Gi7/7" == interface_converter("g7/7", "ios")  # noqa
    assert "Eth7/7" == interface_converter("g7/7", "eos")  # noqa
    assert "xe-7/7" == interface_converter("xe-7/7", "ios")  # noqa
    with pytest.raises(ValueError, match="Interface not understood"):
        interface_converter("g7/7", "dummy")  # noqa


def test_link_creation():
    link = Link("r1", "Gi0/0", "r2", "Gi0/0")  # noqa
    assert link.device_a == "r1"
    assert link.interface_a == "Gi0/0"
    assert link.device_b == "r2"
    assert link.interface_b == "Gi0/0"


def test_links_creation():
    l1 = Link("r1", "Gi0/0", "r2", "Gi0/0")  # noqa
    l2 = Link("r1", "Gi1/1", "r2", "Gi1/1")  # noqa
    links = Links([l1, l2])  # noqa
    assert l1 in links
    assert l2 in links
    assert len(links) == 2


def test_links_manipulation():
    links = Links()  # noqa
    links.append(Link("r1", "Gi0/0", "r2", "Gi0/0"))  # noqa
    assert len(links) == 1
    assert links[0].device_a == "r1"
    assert links[0].interface_a == "Gi0/0"
    l3 = Link("r1", "Gi1/2", "r3", "Gi1/2")  # noqa
    links.extend([Link("r1", "Gi1/1", "r2", "Gi1/1"), l3])  # noqa
    assert len(links) == 3
    l4 = Link("r4", "Gi1/2", "r5", "Gi1/2")  # noqa
    assert l4 not in links


def test_links_with_wrong_values():
    links = Links()  # noqa
    with pytest.raises(ValueError, match="Value is not of Link type: dummy"):
        links.append("dummy")  # type: ignore
    l1 = Link("r1", "Gi0/0", "r2", "Gi0/0")  # noqa
    links.extend([l1])
    with pytest.raises(ValueError, match="Link is already used"):
        links.insert(1, l1)
    with pytest.raises(ValueError, match="Link is already used"):
        links.append(l1)
    with pytest.raises(ValueError, match="Link is already used"):
        links.extend([l1])


def test_complete_run():
    # flake8: noqa
    facts = read_yaml(FACTS)
    cutsheet = read_csv(CUTSHEET)
    devices = Devices({dev["device"]: Device(**dev) for dev in facts})
    links = links_fabric(cutsheet, devices, skip_errors=True)
    assert repr(devices) == "Devices('r1', 'r2', 'r3', 'r4', 'r5', 'r6')"
    assert links == [
        Link(device_a="r1", interface_a="Gi0/0", device_b="r2", interface_b="Gi0/0"),
        Link(device_a="r1", interface_a="Gi0/1", device_b="r3", interface_b="Gi0/0"),
        Link(device_a="r2", interface_a="Gi0/1", device_b="r3", interface_b="Gi0/1"),
        Link(device_a="r3", interface_a="Gi1/0", device_b="r4", interface_b="Eth1/0"),
        Link(device_a="r2", interface_a="Gi1/0", device_b="r5", interface_b="Eth1/0"),
        Link(device_a="r4", interface_a="Eth0/1", device_b="r5", interface_b="Eth0/1"),
        Link(device_a="r6", interface_a="Eth0/0", device_b="r4", interface_b="Eth0/0"),
        Link(device_a="r6", interface_a="Eth0/1", device_b="r5", interface_b="Eth0/0"),
    ]

